MAKE    ?= make
RACKET  := racket
RACO    := $(RACKET) -l raco --
REQ     := $(RACO) req
FROG    := $(RACO) frog

PWD     := $(shell pwd)
PUBLIC  := $(PWD)/public


.PHONY: all
all: public

.PHONY: install-req
install-req:
	$(RACO) pkg install --auto --no-docs --skip-installed req-lib

.PHONY: req
req: install-req
req:
	$(REQ) --verbose --everything

.PHONY: frog-build
frog-build:
	$(FROG) --very-verbose --build

.PHONY: frog-clean
frog-clean:
	$(FROG) --very-verbose --clean

.PHONY: clean
clean:
	$(MAKE) frog-clean
	$(MAKE) -C public clean

$(PUBLIC):
	$(MAKE) frog-build

.PHONY: public
public:
	$(MAKE) -B $(PUBLIC)
