# About Gentoo-Racket

Gentoo-Racket is a unofficial Gentoo project that aims to bring first-class
support for Racket programming language ecosystem to Gentoo.

## Notable projects

- [Gentoo-Racket-Overlay](https://gitlab.com/gentoo-racket/gentoo-racket-overlay/)
  ---
  Gentoo overlay with Racket packages
- [Racket-Collector2](https://gitlab.com/gentoo-racket/racket-collector2/)
  ---
  generate Gentoo ebuilds from the Racket Catalog
- [Racket-Ebuild](https://gitlab.com/gentoo-racket/racket-ebuild/)
  ---
  library to ease automatic ebuild creation
- [Racket-VDB](https://gitlab.com/gentoo-racket/racket-vdb/)
  ---
  Racket interface to the Portage VDB
- [Racket-PMSF](https://gitlab.com/gentoo-racket/racket-pmsf/)
  ---
  Package Manager Specification formatting and parsing
- [Racket-Eclass2Scrbl](https://gitlab.com/gentoo-racket/racket-eclass2scrbl/)
  ---
  convert Gentoo Eclasses to Scribble documents
- [Racket-Where](https://gitlab.com/gentoo-racket/racket-where/)
  ---
  small tool to find install locations of Racket packages
