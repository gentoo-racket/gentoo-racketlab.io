# Contributors to Gentoo-Racket

This is a list of people who have contributed to the Gentoo-Racket project.

- Maciej Barć
    - Area: project leader
    - Mail: [xgqt@gentoo.org](mailto:xgqt@gentoo.org)
    - GitLab: [xgqt](https://gitlab.com/xgqt/)
- Tom Gillespie
    - GitLab: [tgbugs](https://gitlab.com/tgbugs/)
- Alfred Windgate
    - GitLab: [Parona](https://gitlab.com/Parona/)

Above list is extended periodically by the project leader or a developer
with access to the `gentoo-racket-website` repository.
Because to contribute to Gentoo-Racket most of the time
(except in the case of emailing patches) a GitLab account is needed
the account of the contributor is added to the list.
