    Title: New Racket-PMSF release - version 2.0.0
    Date: 2023-02-09T00:00:00
    Tags: ebuild, gentoo, manifest, release

New Racket-PMSF release is available:
[2.0.0](https://gitlab.com/gentoo-racket/racket-pmsf/-/tree/2.0.0/).

Racket-PMSF is a library implementing parsing of special syntax defined in the
[Package Manager Specification](https://dev.gentoo.org/~ulm/pms/head/pms.html)
document.

Version 2.0.0 is now able to parse Gentoo Manifest files.
Manifest support is based on
the [GLEP 74](https://www.gentoo.org/glep/glep-0074.html) document.
New release also features a rewrite of all tokenizers to use the `brag/support`
library instead of hacky regex/string-split soup.

To read about currently available Manifest-oriented procedures, see
the [Manifest](https://gentoo-racket.gitlab.io/racket-pmsf/pmsf-manifest.html)
section in the PMSF documentation.

PMSF documentation is available via GitLab pages at
[gentoo-racket.gitlab.io/racket-pmsf](https://gentoo-racket.gitlab.io/racket-pmsf/)
or on the official Racket Documentation website at
[docs.racket-lang.org/pmsf](https://docs.racket-lang.org/pmsf/).
