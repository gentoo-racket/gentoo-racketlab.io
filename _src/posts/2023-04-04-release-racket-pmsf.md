    Title: New Racket-PMSF release - version 3.2
    Date: 2023-04-04T00:00:00
    Tags: ebuild, gentoo, release

New Racket-PMSF release is available:
[3.2](https://gitlab.com/gentoo-racket/racket-pmsf/-/tree/3.2/).

Version 3.2 is now able to parse PMS `LICENSE` syntax.

Also, a experimental support for full Portage ebuild cache parsing is
available.
