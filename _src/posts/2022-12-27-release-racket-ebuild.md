    Title: New Racket-Ebuild release - version 16.1.0
    Date: 2022-12-27T00:00:00
    Tags: gentoo, ebuild, release

New Racket-Ebuild release is available:
[16.1.0](https://gitlab.com/gentoo-racket/racket-ebuild/-/tree/16.1.0/).
