    Title: New Racket-PMSF release - version 1.0.0
    Date: 2022-10-29T00:00:00
    Tags: gentoo, ebuild, release

New Racket-PMSF release is available:
[1.0.0](https://gitlab.com/gentoo-racket/racket-pmsf/-/tree/1.0.0/).

Documentation is available via GitLab pages at
[gentoo-racket.gitlab.io/racket-pmsf](https://gentoo-racket.gitlab.io/racket-pmsf/).

Racket-PMSF 1.0.0. is now able to parse all special syntax structures
from ebuilds:

- `DEPEND`

```racket
(string->pdepend "asd? ( || ( app-misc/asd:= >=app-misc/dsa-1:1=[asd] ) )")
```
```racket
(pdepend
 (list
  (pcondition
   "asd"
   (list
    (pcondition
     'inclusive-or
     (list
      (pdependency #f #f (pcomplete "app-misc" (pfull "asd" #f)) #f #f "=" '())
      (pdependency
       #f
       ">="
       (pcomplete
        "app-misc"
        (pfull "dsa" (pversion "1" '() '() '() '() '() 0)))
       #f
       (pslot "1" #f)
       "="
       (list (pdepflag "asd" #f #f)))))))))
```

- `REQUIRED_USE`

```racket
(string->prequired-use "test? ( debug )")
```
```racket
(prequired-use (list (pcondition "test" '("debug"))))
```

- `RESTRICT`


```racket
(string->prestrict "!test? ( test )")
```
```racket
(prestrict (list (pcondition '(not . "test") '("test"))))
```

- `SLOT`

```racket
(string->pslot "1.2.3/4")
```
```racket
(pslot "1.2.3" "4")
```

- `SRC_URI`

```racket
(string->psrc-uri "amd64? ( https://asd.asd/asd-0_x86.tar -> asd-0.tar )")
```
```racket
(psrc-uri
 (list
  (pcondition
   "amd64"
   (list
    (psource
     (url
      "https"
      #f
      "asd.asd"
      #f
      #t
      (list (path/param "asd-0_x86.tar" '()))
      '()
      #f)
     "asd-0.tar")))))
```

- package names

```racket
(string->pcomplete "app-misc/editor-wrapper-1.2.3_alpha4_beta5_pre6_rc7_p8-r9")
```
```racket
(pcomplete "app-misc"
           (pfull "editor-wrapper"
                  (pversion "1.2.3" '("4") '("5") '("6") '("7") '("8") 9)))
```
