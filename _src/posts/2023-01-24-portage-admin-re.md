    Title: Rewrite of Racket-Portage-Admin's buggy component and relicense
    Date: 2023-01-24T19:00:00
    Tags: gentoo, portage

Today
[Racket-Portage-Admin](https://gitlab.com/gentoo-racket/racket-portage-admin/)
had a buggy component responsible for emerge log file snooping
[rewritten](https://gitlab.com/gentoo-racket/racket-portage-admin/-/commit/fb84bb6cedf1543e7e4d6a411bdc3225b747e272/).

Also, Racket-Portage-Admin was
[relicensed](https://gitlab.com/gentoo-racket/racket-portage-admin/-/commit/84fc9d2e2838eca9d576ca0f6862ed0e10841c86/)
to the GPL-2+ license.
