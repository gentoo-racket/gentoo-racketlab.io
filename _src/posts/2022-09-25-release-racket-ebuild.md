    Title: New Racket-Ebuild release - version 16.0.3
    Date: 2022-09-25T00:00:00
    Tags: gentoo, ebuild, release

New Racket-Ebuild release is available:
[16.0.3](https://gitlab.com/gentoo-racket/racket-ebuild/-/tree/16.0.3/).
