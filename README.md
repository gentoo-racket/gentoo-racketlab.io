# Gentoo-Racket.GitLab.IO

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/gentoo-racket.gitlab.io/">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/gentoo-racket.gitlab.io/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/gentoo-racket.gitlab.io/pipelines">
        <img src="https://gitlab.com/gentoo-racket/gentoo-racket.gitlab.io/badges/master/pipeline.svg">
    </a>
</p>

Gentoo Racket project's GitLab pages.


## About

You can read more [on GitLab pages](https://gentoo-racket.gitlab.io/).
